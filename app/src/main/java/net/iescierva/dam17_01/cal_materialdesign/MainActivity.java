package net.iescierva.dam17_01.cal_materialdesign;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public TextView Display;
    public TextView Porcentaje;
    public double num1, num2, result;
    boolean op2=false;
    boolean Porciento=false;
    boolean punt=false;
    int op;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingActionButton2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        setContentView(R.layout.activity_main);
        Display=(TextView)findViewById(R.id.textView);
        Porcentaje=(TextView)findViewById(R.id.Porcentaje);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void btn1(View v){

        String conca=Display.getText().toString();
        conca+="1";
        Display.setText(conca);

    }

    public void btn2(View v){

        String conca=Display.getText().toString();
        conca+="2";
        Display.setText(conca);

    }

    public void btn3(View v){

        String conca=Display.getText().toString();
        conca+="3";
        Display.setText(conca);

    }

    public void btn4(View v){

        String conca=Display.getText().toString();
        conca+="4";
        Display.setText(conca);

    }

    public void btn5(View v){

        String conca=Display.getText().toString();
        conca+="5";
        Display.setText(conca);

    }

    public void btn6(View v){

        String conca=Display.getText().toString();
        conca+="6";
        Display.setText(conca);

    }

    public void btn7(View v){

        String conca=Display.getText().toString();
        conca+="7";
        Display.setText(conca);

    }

    public void btn8(View v){

        String conca=Display.getText().toString();
        conca+="8";
        Display.setText(conca);

    }

    public void btn9(View v){

        String conca=Display.getText().toString();
        conca+="9";
        Display.setText(conca);

    }

    public void btn0(View v){

        String conca=Display.getText().toString();
        conca+="0";
        Display.setText(conca);

    }

    public void btnComa(View v) {

        if (punt==false) {
            String conca = Display.getText().toString();
            conca += ".";
            Display.setText(conca);
            punt = true;
        }
    }

    public void btnPorcentajeentoento(View v){

        if (Porcentaje.getText() == "") {

            Porcentaje.setText("%");

        }
        else {

            Porcentaje.setText("");

        }
    }

    public void suma (View v) {

        if (op2 == false) {
            try {

                if (Porcentaje.getText() == "") {

                    String aux1 = Display.getText().toString();
                    num1 = Double.parseDouble(aux1);
                    punt = false;
                    op2 = true;
                }
            } catch (NumberFormatException nfe) {
            }
        } else {
            try {
                String aux2 = Display.getText().toString();
                num2 = Double.parseDouble(aux2);

                if (Porcentaje.getText() == "%"){

                    num2=num1*(num2/100);

                }

                switch (op) {

                    case 1:
                        num1 = num1 + num2;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        break;
                    case 4:
                        if (num2 == Double.NaN) {
                            Display.setText("Error");
                        } else {
                            result = num1 / num2;

                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "No hay oprandos suficientes", Toast.LENGTH_SHORT);
            }
        }
        Display.setText("");
        op = 1;
    }

    public void resta (View v){

        if (op2==false) {
            try {

                if (Porcentaje.getText() == "") {

                    String aux1 = Display.getText().toString();
                    num1 = Double.parseDouble(aux1);
                    punt = false;
                    op2=true;

                }

            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux2 = Display.getText().toString();
                num2 = Double.parseDouble(aux2);

                if (Porcentaje.getText() == "%"){

                    num2=num1*(num2/100);

                }

                switch (op) {

                    case 1:
                        num1 = num1 + num2;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            Display.setText("Error");
                        }
                        else{
                            result=num1/num2;

                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "No hay oprandos suficientes", Toast.LENGTH_SHORT);
            }
        }
        Display.setText("");
        op=2;

    }

    public void multi (View v){

        if (op2==false) {
            try {

                if (Porcentaje.getText() == "") {

                    String aux1 = Display.getText().toString();
                    num1 = Double.parseDouble(aux1);
                    punt = false;
                    op2=true;

                }
            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux2 = Display.getText().toString();
                num2 = Double.parseDouble(aux2);

                if (Porcentaje.getText() == "%"){

                    num2=(num2/100);

                }

                switch (op) {

                    case 1:
                        num1 = num1 + num2;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            Display.setText("Error");
                        }
                        else{
                            result=num1/num2;

                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "No hay oprandos suficientes", Toast.LENGTH_SHORT);
            }
        }
        Display.setText("");
        op=3;

    }

    public void divi (View v){

        if (op2==false) {
            try {

                if (Porcentaje.getText() == "") {

                    String aux1 = Display.getText().toString();
                    num1 = Double.parseDouble(aux1);
                    punt = false;
                    op2=true;

                }
            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux2 = Display.getText().toString();
                num2 = Double.parseDouble(aux2);

                if (Porcentaje.getText() == "%"){

                    num2=num1*(num2/100);

                }

                switch (op) {

                    case 1:
                        num1 = num1 + num2;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            Display.setText("Error");
                        }
                        else{
                            result=num1/num2;

                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "No hay oprandos suficientes", Toast.LENGTH_SHORT).show();
            }
        }
        Display.setText("");
        op=4;

    }

    public void exponente(View v){


        if (op2==false) {
            try {

                if (Porcentaje.getText() == "") {

                    String aux1 = Display.getText().toString();
                    num1 = Double.parseDouble(aux1);
                    punt = false;
                    op2=true;

                }
            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux2 = Display.getText().toString();
                num2 = Double.parseDouble(aux2);

                if (Porcentaje.getText() == "%"){

                    num2=num1*(num2/100);

                }

                switch (op) {

                    case 1:
                        num1 = num1 + num2;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            Display.setText("Error");
                        }
                        else{
                            result=num1/num2;

                        }
                        break;
                    case 5:
                        num1= num1 * Math.pow(10 , num2);
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "No hay oprandos suficientes", Toast.LENGTH_SHORT).show();
            }
        }
        Display.setText("");
        op=5;



    }

    public void igual(View v){

        try{

            String aux2=Display.getText().toString();
            num2=Double.parseDouble(aux2);
            punt=false;
            op2=false;

            if (Porcentaje.getText() == "%" && op!=3){

                num2=num1*(num2/100);

            }
            if(Porcentaje.getText() == "%" && op==3){

                num2=num2/100;

            }


        }catch(NumberFormatException nfe){
        }

        Display.setText("");

        if (op==1){
            result= num1+num2;
        }

        else if (op==2){
            result= num1-num2;
        }

        else if (op==3){
            result= num1*num2;
        }

        else if (op==4){

            if (num2==Double.NaN) {
                Display.setText("Error");
            }
            else
                result=num1/num2;

        }
        else if (op==5){

            result= num1 * Math.pow(10 , num2);

        }

        Display.setText(""+result);
        num1=result;
        Porcentaje.setText("");
    }

    public void clear(View v){

        Display.setText("");
        num1=0.0;
        num2=0.0;
        result=0.0;

        op2=false;
        punt=false;

    }

    public void borrar(View v){

        if (!Display.getText().toString().equals("")){
            Display.setText("");
        }
        punt=false;

    }


}
